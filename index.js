const registerSchema = new mongoose.Schema({
    username: String,
    status:{
        type: String,
        default: "pending"
    }
})

const Register = mongoose.model("Register", registerSchema)

app.post("/registration", (req, res) => {
    Register.findOne( { name:req.body.name }, (error, result) => {
        if( result !== null && result.name === req.body.name ){
            return res.send("username already in use");
        } else {
            let newRegister = new Register ({
                name: req.body.name
            })
            newRegister.save((saveErr, savedTask) => {
                if (saveErr){
                    return console.error(saveErr);
                } else {
                    return res.status(201).send("successfully registered")
                }
            })
        }
    })
})

app.get("/registration", (req, res) => {
    Register.find( {}, (error, result) => {
        if (error) {
            return console.log(error)
        }else{
            return res.status(200).json({data:result})
        }
    })
})

